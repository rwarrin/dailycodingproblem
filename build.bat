@echo off

SET CompilerFlags=/nologo /Z7 /Od /fp:fast
SET LinkerFlags=/incremental:no

cl.exe %CompilerFlags% %1 /link %LinkerFlags%
