/**
 * This is your coding interview problem for today.
 * 
 * This problem was asked by Dropbox.
 * 
 * Given the root to a binary search tree, find the second largest node in the
 * tree.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

struct node
{
	int Value;
	struct node *Left;
	struct node *Right;
};

static void
Insert(int Value, struct node **Tree)
{
	struct node *NewNode = (struct node *)malloc(sizeof(*NewNode));
	NewNode->Left = 0;
	NewNode->Right = 0;
	NewNode->Value = Value;

	if(*Tree == NULL)
	{
		*Tree = NewNode;
	}
	else
	{
		struct node *Walker = *Tree;
		while(Walker != NULL)
		{
			if(Value == Walker->Value)
			{
				free(NewNode);
				break;
			}
			else if(Value > Walker->Value)
			{
				if(Walker->Right == NULL)
				{
					Walker->Right = NewNode;
					break;
				}
				else
				{
					Walker = Walker->Right;
				}
			}
			else if(Value < Walker->Value)
			{
				if(Walker->Left == NULL)
				{
					Walker->Left = NewNode;
					break;
				}
				{
					Walker = Walker->Left;
				}
			}
		}
	}
}

#define FindIthLargestValue(I, Tree) _FindIthLargestValue(I, Tree, NULL, NULL)
static int
_FindIthLargestValue(int I, struct node *Tree, int *Index, int *Array)
{
	if((Index != 0) && 
	   (*Index >= I))
	{
		return 0;
	}

	if(Tree == 0)
	{
		return 0;
	}

	if(Array == NULL)
	{
		Array = (int *)malloc(sizeof(*Array) * I);
		Index = (int *)malloc(sizeof(int));
		*Index = 0;
	}

	_FindIthLargestValue(I, Tree->Right, Index, Array);
	Array[*Index] = Tree->Value;
	*Index = *Index + 1;
	_FindIthLargestValue(I, Tree->Left, Index, Array);

	return Array[I - 1];
}

int
main(void)
{
	struct node *Tree = 0;
	int Numbers[] = {5, 4, 8, 9, 3, 1, 2, 7, 0, 6};
	for(int i = 0; i < ArrayCount(Numbers); i++)
	{
		Insert(Numbers[i], &Tree);
	}

	int I = 2;
	int Value = FindIthLargestValue(I, Tree);
	printf("%d is the %dth largest value\n", Value, I);

	return 0;
}
