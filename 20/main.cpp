/**
 * This problem was asked by Google.
 * 
 * Given two singly linked lists that intersect at some point, find the
 * intersecting node. The lists are non-cyclical.
 * 
 * For example, given A = 3 -> 7 -> 8 -> 10 and B = 99 -> 1 -> 8 -> 10, return
 * the node with value 8.
 * 
 * In this example, assume nodes with the same value are the exact same node
 * objects.
 * 
 * Do this in O(M + N) time (where M and N are the lengths of the lists) and
 * constant space.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct node* link;
struct node
{
	int Value;
	link Next;
};

static inline void
PushLink(int Value, link *List)
{
	link Node = (link)malloc(sizeof(*Node));
	if(Node != NULL)
	{
		Node->Value = Value;
		Node->Next = *List;
		*List = Node;
	}
}

static void
FindIntersection(link ListA, link ListB)
{
	int ListALength = 0;
	for(link Node = ListA; Node != 0; Node = Node->Next, ++ListALength) {}

	int ListBLength = 0;
	for(link Node = ListB; Node != 0; Node = Node->Next, ++ListBLength) {}

	link Longest = (ListALength > ListBLength ? ListA : ListB);
	link Shortest = (Longest == ListA ? ListB : ListA);

	int Difference = ListALength - ListBLength;
	if(Difference < 0)
	{
		Difference = Difference * -1;
	}

	for(int i = 0; i < Difference; ++i)
	{
		Longest = Longest->Next;
	}

	for(; Longest != Shortest; Longest = Longest->Next, Shortest = Shortest->Next)
	{
		if(Longest == 0)
		{
			break;
		}
	}

	if(Longest == 0)
	{
		printf("Lists do not intersect\n");
	}
	else
	{
		printf("Lists intersect at %d (%p)\n", Longest->Value, Longest);
	}
}

int
main(void)
{
	link Intersection = 0;
	PushLink(10, &Intersection);
	PushLink(8, &Intersection);

	link ListA = Intersection;
	PushLink(7, &ListA);
	PushLink(3, &ListA);
	//PushLink(1, &ListA);

	link ListB = Intersection;
	PushLink(1, &ListB);
	PushLink(99, &ListB);

	FindIntersection(ListA, ListB);

	return 0;
}
