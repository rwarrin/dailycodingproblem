/**
 * This problem was asked by Twitter.
 * 
 * You run an e-commerce website and want to record the last N order ids in a
 * log. Implement a data structure to accomplish this, with the following API:
 * 
 * record(order_id): adds the order_id to the log get_last(i): gets the ith last
 * element from the log. i is guaranteed to be smaller than or equal to N.  You
 * should be as efficient with time and space as possible.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

struct order_log
{
	int Size;
	int At;
	int *Log;
};

static struct order_log
NewLog(int Size)
{
	struct order_log Log = {0};
	Log.Size = Size;
	Log.Log = (int *)malloc(sizeof(int) * Size);
	Log.At = 0;

	return Log;
}

static int
Record(struct order_log *OrderLog, int OrderId)
{
	assert(OrderLog != NULL);

	int At = OrderLog->At % OrderLog->Size;
	OrderLog->Log[At] = OrderId;
	OrderLog->At = ++At;

	return OrderId;
}

static int
GetLast(struct order_log *OrderLog, int Back)
{
	assert(OrderLog != NULL);

	int Result = INT_MIN;

	int Index = OrderLog->At - Back;
	if(Index < 0)
	{
		Index = OrderLog->Size + Index;
	}

	Result = OrderLog->Log[Index];
	return Result;
}

int
main(void)
{
	struct order_log OrderLog = NewLog(10);

	for(int i = 0; i < 17; i++)
	{
		Record(&OrderLog, i);
	}

	for(int i = 0; i < OrderLog.Size; i++)
	{
		printf("%dth entry = %d\n", i, GetLast(&OrderLog, i));
	}

	return 0;
}
