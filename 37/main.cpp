/**
 * This problem was asked by Google.
 * 
 * The power set of a set is the set of all its subsets. Write a function that,
 * given a set, generates its power set.
 * 
 * For example, given the set {1, 2, 3}, it should return {{}, {1}, {2}, {3},
 * {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}.
 * 
 * You may also use a list or array to represent a set.
 *
 * Strings are arrays too! ;)
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

struct set
{
	int Size;
	int Count;
	char **List;
};

static struct set
SetNew(int Size)
{
	assert(Size >= 0);
	if(Size == 0)
	{
		Size = 1;
	}

	struct set Result = {0};
	Result.Size = Size;
	Result.Count = 0;
	Result.List = (char **)malloc(sizeof(char *) * Result.Size);

	return Result;
}

static int
SetAdd(struct set *Set, char *Value)
{
	assert(Set != NULL);

	for(int Index = 0; Index < Set->Count; ++Index)
	{
		if(strcmp(Value, Set->List[Index]) == 0)
		{
			return 0;
		}
	}

	if(Set->Count + 1 >= Set->Size)
	{
		Set->Size = Set->Size * 2;
		char **NewList = (char **)realloc(Set->List, sizeof(*NewList) * Set->Size);
		assert(NewList);
		Set->List = NewList;
	}

	Set->List[Set->Count++] = Value;
	return 1;
}

static char *
StringJoin(char Character, char *String)
{
	int Length = strlen(String);
	char *NewString = (char *)malloc(sizeof(char) * Length + 1);
	assert(NewString != NULL);

	char *Insert = NewString;
	*Insert++ = Character;
	while(*String != 0)
	{
		*Insert++ = *String++;
	}
	*Insert = 0;

	return NewString;
}

static void
GetPowerSet(char **Array, int Size, struct set *Set)
{
	if(Size == 0)
	{
		SetAdd(Set, "");
		return;
	}

	char Character = Array[0][0];
	GetPowerSet(Array + 1, Size - 1, Set);
	for(int Index = 0, Length = Set->Count; Index < Length; ++Index)
	{
		char *NewString = StringJoin(Character, Set->List[Index]);
		int Result = SetAdd(Set, NewString);
		if(Result == 0)
		{
			free(NewString);
		}
	}
}

int
main(void)
{

	char *Array[] = {"1", "2", "3"};
	struct set Set = SetNew(ArrayCount(Array) * ArrayCount(Array));
	GetPowerSet(Array, ArrayCount(Array), &Set);
	for(int Index = 0; Index < Set.Count; ++Index)
	{
		printf("%s\n", Set.List[Index]);
	}

	return 0;
}
