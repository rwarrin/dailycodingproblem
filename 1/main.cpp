#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#if 0
// O(n^2) - trade space for time
// Space of O(1)
bool
ArrayHasSumPair(int *Array, int Length, int TargetSum)
{
	assert(Array != 0);

	for(int i = 0; i < Length - 1; ++i)
	{
		for(int j = i + 1; j < Length; ++j)
		{
			int Sum = Array[i] + Array[j];
			if(Sum == TargetSum)
			{
				return true;
			}
		}
	}

	return false;
}
#else
// Bonus: Can you do it in a single pass?
// This solution uses a Table to store values found and tested in the array.
// For each value in the array we get the value requried to sum up to the
// TargetSum and then look in the table to see if we've seen it already. If we
// have seen the difference then we've found a pair that sums to TargetSum, if
// no then we insert the current value into the table and continue.
// O(n) - we trade time for space.
#define TABLE_SIZE 1024
bool
ArrayHasSumPair(int *Array, int Length, int TargetSum)
{
	assert(Array != 0);

	int HashTable[TABLE_SIZE] = {0};

	for(int Index = 0; Index < Length; ++Index)
	{
		int Difference = TargetSum - Array[Index];
		if((Difference >= 0) && (HashTable[Difference] == 1))
		{
			return true;
		}

		HashTable[Array[Index]] = 1;
	}

	return false;
}
#undef TABLE_SIZE
#endif

int
main(void)
{
	int Array[] = {10, 15, 3, 7};
	int TargetSum = 17;
	printf("Array has sum pair: %s\n", ArrayHasSumPair(Array, (sizeof(Array) / sizeof(Array[0])), TargetSum) ? "Yes" : "No");
	return 0;
}
