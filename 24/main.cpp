/**
 * This is your coding interview problem for today.
 * 
 * This problem was asked by Google.
 * 
 * Implement locking in a binary tree. A binary tree node can be locked or
 * unlocked only if all of its descendants or ancestors are not locked.
 * 
 * Design a binary tree node class with the following methods:
 * 
 * is_locked, which returns whether the node is locked
 *
 * lock, which attempts to lock the node. If it cannot be locked, then it should
 * return false. Otherwise, it should lock it and return true.
 *
 * unlock, which unlocks the node. If it cannot be unlocked, then it should
 * return false. Otherwise, it should unlock it and return true.
 *
 * You may augment the node to add parent pointers or any other property you
 * would like. You may assume the class is used in a single-threaded program, so
 * there is no need for actual locks or mutexes. Each method should run in O(h),
 * where h is the height of the tree.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define true 1
#define false 0
typedef int bool32;

struct node
{
	struct node *Parent;
	struct node *Left;
	struct node *Right;
	int Value;
	int LockedChildrenCount;
	bool32 Locked;
};

static struct node *
InsertValue(int Value, struct node **Tree)
{
	struct node *NewNode = (struct node *)malloc(sizeof(*NewNode));
	if(NewNode != NULL)
	{
		NewNode->Value = Value;
		NewNode->LockedChildrenCount = 0; 
		NewNode->Locked = false; 
		NewNode->Parent = 0;
		NewNode->Left = 0;
		NewNode->Right = 0;

		if(*Tree == NULL)
		{
			*Tree = NewNode;
		}
		else
		{
			struct node *Walker = *Tree;
			while(Walker != 0)
			{
				if(NewNode->Value == Walker->Value)
				{
					free(NewNode);
					break;
				}
				else if(NewNode->Value > Walker->Value)
				{
					if(Walker->Right == 0)
					{
						NewNode->Parent = Walker;
						Walker->Right = NewNode;
						break;
					}
					else
					{
						Walker = Walker->Right;
					}
				}
				else if(NewNode->Value < Walker->Value)
				{
					if(Walker->Left == 0)
					{
						NewNode->Parent = Walker;
						Walker->Left = NewNode;
						break;
					}
					else
					{
						Walker = Walker->Left;
					}
				}
			}
		}
	}

	return NewNode;
}

static bool
IsLocked(struct node *Node)
{
	assert(Node != NULL);
	bool Result = ((Node->Locked == true) ||
				   (Node->LockedChildrenCount > 0));
	return Result;
}

static bool
Lock(struct node *Node)
{
	assert(Node != NULL);

	bool Result = false;
	
	if((Node->Locked == false) &&
	   (Node->LockedChildrenCount == 0))
	{
		Node->Locked = true;
		for(struct node *Walker = Node->Parent; Walker != 0; Walker = Walker->Parent)
		{
			++Walker->LockedChildrenCount;
		}

		Result = true;
	}

	return Result;
}

static bool
Unlock(struct node *Node)
{
	assert(Node != NULL);

	bool Result = false;
	if((Node->Locked == true) &&
	   (Node->LockedChildrenCount == 0))
	{
		Node->Locked = false;
		for(struct node *Walker = Node->Parent; Walker != 0; Walker = Walker->Parent)
		{
			--Walker->LockedChildrenCount;
		}

		Result = true;
	}

	return Result;
}

static void
PrintTree(struct node *Tree)
{
	if(Tree == 0)
	{
		return;
	}

	PrintTree(Tree->Left);
	printf("%3d locked = %s, locked children count = %d\n",
		   Tree->Value, (Tree->Locked == true ? "YES" : "NO"),
		   Tree->LockedChildrenCount);
	PrintTree(Tree->Right);
}

int
main(void)
{
	struct node *Head = 0;
	InsertValue(1, &Head);
	InsertValue(9, &Head);
	InsertValue(5, &Head);
	struct node *Locked1 = InsertValue(4, &Head);
	InsertValue(7, &Head);
	struct node *Locked2 = InsertValue(3, &Head);
	InsertValue(2, &Head);

	PrintTree(Head);
	printf("\n");

	Lock(Locked1);
	PrintTree(Head);
	printf("\n");
	Lock(Locked2);
	PrintTree(Head);
	printf("\n");
	Unlock(Locked1);
	PrintTree(Head);
	printf("\n");
	Unlock(Locked2);
	PrintTree(Head);
	printf("\n");
	Unlock(Locked1);
	PrintTree(Head);
	printf("\n");

	return 0;
}
