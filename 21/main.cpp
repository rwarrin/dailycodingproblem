/**
 * This problem was asked by Snapchat.
 * 
 * Given an array of time intervals (start, end) for classroom lectures
 * (possibly overlapping), find the minimum number of rooms required.
 * 
 * For example, given [(30, 75), (0, 50), (60, 150)], you should return 2
 *
 *
 *  0  10  20  30  40  50  60  70  80  90  100  110  120  130  140  150
 *             *******************
 *  ********************
 *                         ******************************************
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))
#define MAX(A, B) ((A) > (B) ? (A) : (B))

struct time_interval
{
	int Start;
	int End;
};

// O(n^2) gotta loop over the array n times for each item in the array.
static int
FindMinimumRoomsRequiredSlow(struct time_interval *Array, int Length)
{
	int MaxRooms = INT_MIN;

	for(int i = 0; i < Length; i++)
	{
		int Overlaps = 1;
		for(int j = i + 1; j < Length; j++)
		{
			if((Array[j].End >= Array[i].Start) &&
			   (Array[j].End <= Array[i].End))
			{
				++Overlaps;
			}
		}

		MaxRooms = MAX(MaxRooms, Overlaps);
	}

	return MaxRooms;
}

enum time_action
{
	TimeAction_Unknown = 0,
	TimeAction_Start,
	TimeAction_End,
};

struct time_event
{
	time_action EventType;
	int Time;
	struct time_event *Next;
};

static inline void
InsertTimeEvent(int Time, time_action EventType, struct time_event **List)
{
	struct time_event *Event = (struct time_event *)malloc(sizeof(*Event));
	Event->Time = Time;
	Event->EventType = EventType;
	Event->Next = 0;

	if(*List == NULL)
	{
		*List = Event;
	}
	else
	{
		struct time_event *Walker = *List;
		for(; Walker->Next != NULL; Walker = Walker->Next)
		{
			if(Walker->Next->Time > Time)
			{
				break;
			}
		}

		Event->Next = Walker->Next;
		Walker->Next = Event;
		if(Walker == *List)
		{
			*List = Walker;
		}
	}
}

// O(N + K) where N is the Length of Array and K is Length * 2.
// The idea here is to create count the number of overlaps by counting the
// incrementing/decrementing a counter based on time events (start and end).
// This method transforms interval pairs into a sequence of time events using a
// linked list
//    0  10  20  30  40  50  60  70  80  90  100  110  120  130  140  150
// 2             *********   *******
// 1  ************       *****     *************************************
// 0  *                                                                *
// E  S          S       E   S     E                                   E
static int
FindMinimumRoomsRequiredFast(struct time_interval *Array, int Length)
{
	struct time_event *Sequence = 0;
	for(int Index = 0; Index < Length; ++Index)
	{
		InsertTimeEvent(Array[Index].Start, TimeAction_Start, &Sequence);
		InsertTimeEvent(Array[Index].End, TimeAction_End, &Sequence);
	}

	int MaxRooms = INT_MIN;
	int Rooms = 0;
	for(struct time_event *Event = Sequence;
		Event != NULL;
		Event = Event->Next)
	{
		if(Event->EventType == TimeAction_Start)
		{
			++Rooms;
		}
		else if(Event->EventType = TimeAction_End)
		{
			--Rooms;
		}
		else
		{
			assert(!"Invalid Event TimeAction.\n");
		}

		MaxRooms = MAX(MaxRooms, Rooms);
	}

	return MaxRooms;
}

int
main(void)
{
	struct time_interval Intervals[] = {{30, 75}, {0, 50}, {60, 150}};
	//struct time_interval Intervals[] = {{10, 20}, {30, 40}, {60, 150}};

	int SlowRooms = FindMinimumRoomsRequiredSlow(Intervals, ArrayCount(Intervals));
	printf("FindMinimumRoomsRequiredSlow(): %d rooms required.\n", SlowRooms);

	int FastRooms = FindMinimumRoomsRequiredFast(Intervals, ArrayCount(Intervals));
	printf("FindMinimumRoomsRequiredFast(): %d rooms required.\n", FastRooms);

	return 0;
}
