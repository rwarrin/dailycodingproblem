/**
 * This problem was asked by Google.
 * 
 * Given a singly linked list and an integer k, remove the kth last element from
 * the list. k is guaranteed to be smaller than the length of the list.
 * 
 * The list is very long, so making more than one pass is prohibitively
 * expensive.
 * 
 * Do this in constant space and in one pass.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct node
{
	int Value;
	struct node *Next;
};

static void
AddNode(struct node **List, int Value)
{
	struct node *NewNode = (struct node *)malloc(sizeof(*NewNode));
	NewNode->Value = Value;
	NewNode->Next = *List;
	*List = NewNode;
}

static void
PrintList(struct node *List)
{
	for(; List != 0; List = List->Next)
	{
		printf("%d, ", List->Value);
	}
	printf("\n");
}

static void
RemoveKthLastNode(int K, struct node **List)
{
	struct node *Fast = *List;
	struct node *Slow = *List;
	for(int i = 0; i <= K; i++, Fast = Fast->Next) ;
	printf("Slow: %d, Fast %d\n", Slow->Value, Fast->Value);

	for(; Fast->Next != 0; Fast = Fast->Next, Slow = Slow->Next) ;
	printf("Slow: %d, Fast %d\n", Slow->Value, Fast->Value);

	struct node *Temp = Slow;
	Slow->Next = Slow->Next->Next;
	free(Temp);
}

int
main(void)
{
	struct node *List = 0;
	for(int Size = 100; Size > 0; --Size)
	{
		AddNode(&List, Size);
	}

	PrintList(List);
	RemoveKthLastNode(10, &List);
	PrintList(List);

	return 0;
}
