/**
 * This problem was asked by Palantir.
 * 
 * Write an algorithm to justify text. Given a sequence of words and an integer
 * line length k, return a list of strings which represents each line, fully
 * justified.
 * 
 * More specifically, you should have as many words as possible in each line.
 * There should be at least one space between each word. Pad extra spaces when
 * necessary so that each line has exactly length k. Spaces should be
 * distributed as equally as possible, with the extra spaces, if any,
 * distributed starting from the left.
 * 
 * If you can only fit one word on a line, then you should pad the right-hand
 * side with spaces.
 * 
 * Each word is guaranteed not to be longer than k.
 * 
 * For example, given the list of words ["the", "quick", "brown", "fox",
 * "jumps", "over", "the", "lazy", "dog"] and k = 16, you should return the
 * following:
 * 
 * ["the  quick brown", # 1 extra space on the left
 * "fox  jumps  over", # 2 extra spaces distributed evenly
 * "the   lazy   dog"] # 4 extra spaces distributed evenly
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

static char *
GetJustifiedLine(char **WordList, int LineLength, int *WordsUsed)
{
	if(*WordList == 0)
	{
		return 0;
	}

	char *String = (char *)malloc(sizeof(*String) * LineLength + 1);

	char *WordBuffer[16] = {0};
	int WordLengths[16] = {0};
	int WordBufferLength = 0;
	int TotalWordLength = 0;
	for(char **Word = WordList; *Word != 0; ++Word)
	{
		int Length = strlen(*Word);
		if(TotalWordLength + Length > LineLength)
		{
			break;
		}
		TotalWordLength += Length + 1;  // +1 for space
		WordLengths[WordBufferLength] = Length;
		WordBuffer[WordBufferLength++] = *Word;
	}
	--TotalWordLength;

	int SpacesAvailable = WordBufferLength - 1;
	int SpacesToInsert = LineLength - TotalWordLength;
	int SpacesToInsertPerWord = ceil((float)SpacesToInsert / (float)SpacesAvailable);

	char *Writer = String;
	for(int WordIndex = 0; WordIndex < WordBufferLength; ++WordIndex)
	{
		strcpy(Writer, WordBuffer[WordIndex]);
		Writer += WordLengths[WordIndex];
		*Writer++ = ' ';

		for(int i = 0; i < SpacesToInsertPerWord && SpacesToInsert > 0; ++i, --SpacesToInsert)
		{
			*Writer++ = ' ';
		}
	}

	*WordsUsed = WordBufferLength;
	String[LineLength] = 0;
	return String;
}

static void
Test(int LineLength, char **WordList)
{
	char *Line = 0;
	int WordsUsed = 0;
	while((Line = GetJustifiedLine(WordList, LineLength, &WordsUsed)) != 0)
	{
		printf("%s\n", Line);
		WordList += WordsUsed;
		WordsUsed = 0;
		free(Line);
	}
}

int
main(void)
{
	char *Words[] = {"the", "quick", "brown", "fox", "jumps", "over", "the",
					 "lazy", "dog", NULL};
	Test(16, Words);

	return 0;
}
