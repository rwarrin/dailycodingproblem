/**
 * This problem was asked by Google.
 * 
 * You are given an M by N matrix consisting of booleans that represents a
 * board. Each True boolean represents a wall. Each False boolean represents a
 * tile you can walk on.
 * 
 * Given this matrix, a start coordinate, and an end coordinate, return the
 * minimum number of steps required to reach the end coordinate from the start.
 * If there is no possible path, then return null. You can move up, left, down,
 * and right. You cannot move through walls. You cannot wrap around the edges of
 * the board.
 * 
 * For example, given the following board:
 * 
 * [[f, f, f, f],
 * [ t, t, f, t],
 * [ f, f, f, f],
 * [ f, f, f, f]]
 * and start = (3, 0) (bottom left) and end = (0, 0) (top left), the minimum
 * number of steps required to reach the end is 7, since we would need to go
 * through (1, 2) because there is a wall everywhere else on the second row.
 *
 * I solved this using a Queue, first pushing the Starting cell onto the queue.
 * I then keep popping off the queue while there are items; for each item popped
 * off the queue I see if it's NSEW neighbor is an Open cell and if it is I push
 * it on to the queue. Each pushed queue member tracks it's own Step count so by
 * the time I reach the target X,Y cell I know how many steps I've taken, and
 * that is the minimum number of steps required to get to that cell. This is a
 * sort of floodfill algorithm.
 *
 * I tried to speed things up by using a linked list and a free list to avoid
 * malloc as much as possible. I also Reserve queue links ahead of time and
 * creating the free list of nodes.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

struct grid
{
	unsigned int Width;
	unsigned int Height;
	int *Grid;

	int* At(int X, int Y)
	{
		if((X < 0 || X >= Width) ||
		   (Y < 0 || Y >= Height))
		{
			return 0;
		}

		int *Result = (this->Grid + ((Y * Width) + X));
		return Result;
	}
};

enum queue_node_status
{
	QueueNodeStatus_Uninitialized = 0,
	QueueNodeStatus_Initialized,
};

struct queue_node
{
	int QueueNodeStatus;
	int X;
	int Y;
	int Value;
	int *Cell;
	struct queue_node *Next;
	struct queue_node *Prev;
};

struct queue
{
	struct queue_node *Head;
	struct queue_node *Tail;
	struct queue_node *FreeList;

	void Reserve(int Size);
	void Push(int *Cell, int X, int Y, int Value);
	struct queue_node Pop();
	int Empty();
};

void
queue::Reserve(int Size)
{
	assert(Size > 0);
	for(; Size != 0; --Size)
	{
		struct queue_node *NewNode = (struct queue_node *)malloc(sizeof(struct queue_node));
		NewNode->Prev = 0;
		NewNode->Next = this->FreeList;
		this->FreeList = NewNode;
	}
}

void
queue::Push(int *Cell, int X, int Y, int Value)
{
	if(*Cell == 0)
	{
		return;
	}

	struct queue_node *Node = 0;
	if(this->FreeList == 0)
	{
		this->FreeList = (struct queue_node *)malloc(sizeof(struct queue_node));
		this->FreeList->Next = 0;
		this->FreeList->Prev = 0;
	}

	Node = this->FreeList;
	this->FreeList = this->FreeList->Next;
	Node->Next = 0;
	Node->Prev = 0;

	Node->QueueNodeStatus = QueueNodeStatus_Initialized;
	Node->X = X;
	Node->Y = Y;
	Node->Value = Value;
	Node->Cell = Cell;

	if(this->Head == 0)
	{
		this->Head = Node;
		this->Tail = Node;
	}
	else
	{
		Node->Prev = this->Tail;
		this->Tail->Next = Node;
		this->Tail = Node;
	}
}

struct queue_node
queue::Pop()
{
	struct queue_node Result = {0};

	if(this->Head != 0)
	{
		Result = *(this->Head);

		struct queue_node *Temp = this->Head;
		this->Head = this->Head->Next;

		Temp->Next = this->FreeList;
		this->FreeList = Temp;

		if(this->Head == 0)
		{
			this->Tail = 0;
		}
	}

	return Result;
}

int
queue::Empty()
{
	int Result = 0;
	if(this->Head == 0)
	{
		Result = 1;
	}
	return Result;
}

static struct grid
CreateBoard(unsigned int Width, unsigned Height, char *Configuration)
{
	struct grid Result = {0};
	Result.Width = Width;
	Result.Height = Height;
	Result.Grid = (int *)malloc(sizeof(int) * (Width * Height));

	for(int *Cell = Result.Grid; *Configuration != 0; ++Configuration, ++Cell)
	{
		*Cell = (*Configuration == 'f' ? INT_MAX : INT_MIN);
	}

	return Result;
}

static int
GetMovesToWalkPath(unsigned int StartX, unsigned int StartY,
				   unsigned int EndX, unsigned int EndY,
				   struct grid *Grid)
{
	int Steps = 0;

	struct queue Queue = {0};
	Queue.Reserve((Grid->Width * Grid->Height) * 4 + 1);

	Queue.Push(Grid->At(StartX, StartY), StartX, StartY, 0);
	while(!Queue.Empty())
	{
		struct queue_node Node = Queue.Pop();
		assert(Node.QueueNodeStatus == QueueNodeStatus_Initialized);

		if((Node.X == EndX) && (Node.Y == EndY))
		{
			Steps = Node.Value;
			break;
		}

		int *Temp = 0;
		if(((Temp = Grid->At(Node.X, Node.Y - 1)) != 0) && (*Temp == INT_MAX))
		{
			Queue.Push(Grid->At(Node.X, Node.Y - 1), Node.X, Node.Y - 1, Node.Value + 1);
		}
		if(((Temp = Grid->At(Node.X, Node.Y + 1)) != 0) && (*Temp == INT_MAX))
		{
			Queue.Push(Grid->At(Node.X, Node.Y + 1), Node.X, Node.Y + 1, Node.Value + 1);
		}
		if(((Temp = Grid->At(Node.X - 1, Node.Y)) != 0) && (*Temp == INT_MAX))
		{
			Queue.Push(Grid->At(Node.X - 1, Node.Y), Node.X - 1, Node.Y, Node.Value + 1);
		}
		if(((Temp = Grid->At(Node.X + 1, Node.Y)) != 0) && (*Temp == INT_MAX))
		{
			Queue.Push(Grid->At(Node.X + 1, Node.Y), Node.X + 1, Node.Y, Node.Value + 1);
		}
	}

	return Steps;
}

int
main(void)
{
	struct grid Grid = CreateBoard(4, 4, "ffffttftffffffff");
	int Moves = GetMovesToWalkPath(0, 0, 0, 3, &Grid);
	printf("%d Steps\n", Moves);

	/**
	 * fffffS
	 * fftfff
	 * ftftff
	 * ffffft
	 * ftttff
	 * fEffft
	 * Shortest distance is 9
	 **/
	struct grid Grid2 = CreateBoard(6, 6, "fffffffftfffftftffffffftftttffffffft");
	int Moves2 = GetMovesToWalkPath(5, 0, 1, 5, &Grid2);
	printf("%d Steps\n", Moves2);

	return 0;
}
