/**
 * This is your coding interview problem for today.
 * 
 * This problem was asked by Google.
 * 
 * Given an array of integers and a number k, where 1 <= k <= length of the
 * array, compute the maximum values of each subarray of length k.
 * 
 * For example, given array = [10, 5, 2, 7, 8, 7] and k = 3, we should get: [10, 7, 8, 8], since:
 * 
 * 10 = max(10, 5, 2)
 * 7 = max(5, 2, 7)
 * 8 = max(2, 7, 8)
 * 8 = max(7, 8, 7)
 * Do this in O(n) time and O(k) space. You can modify the input array in-place
 * and you do not need to store the results. You can simply print them out as
 * you compute them.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

#if 0
// Bruteforce method
// O(N * K) solution
static void
FindMaxValuesInArrayRanges(int Range, int *Array, int Length)
{
	for(int i = 0; i + Range <= Length; i++)
	{
		int Max = INT_MIN;
		for(int j = i; j < i + Range; j++)
		{
			if(Array[j] > Max)
			{
				Max = Array[j];
			}
		}
		printf("%d, ", Max);
	}
}
#else
struct queue_node 
{
	int Value;
	struct queue_node *Prev;
	struct queue_node *Next;
};

struct queue
{
	int Count;
	struct queue_node *Head;
	struct queue_node *Tail;

	struct queue_node *FreeList;
};

static struct queue
NewQueue(int Size)
{
	struct queue Queue = {0};
	for(int i = 0; i < Size; i++)
	{
		struct queue_node *Node = (struct queue_node *)malloc(sizeof(*Node));
		Node->Next = Queue.FreeList;
		Queue.FreeList = Node;
	}

	return Queue;
}
#endif

static void
PushFront(struct queue *Queue, int Value)
{
	assert(Queue != NULL);
	assert(Queue->FreeList != 0);

	struct queue_node *Node = Queue->FreeList;
	Queue->FreeList = Queue->FreeList->Next;

	Node->Next = 0;
	Node->Prev = 0;
	Node->Value = Value;

	if(Queue->Head != 0)
	{
		Queue->Head->Prev = Node;
		Node->Next = Queue->Head;
	}
	Queue->Head = Node;
	
	if(Queue->Tail == 0)
	{
		Queue->Tail = Node;
	}
}

static void
PopFront(struct queue *Queue)
{
	assert(Queue != 0);

	struct queue_node *Temp = Queue->Head;
	Queue->Head = Queue->Head->Next;
	if(Queue->Head != 0)
	{
		Queue->Head->Prev = 0;
	}
	else
	{
		Queue->Tail = 0;
	}

	Temp->Value = -1;
	Temp->Prev = 0;
	Temp->Next = Queue->FreeList;
	Queue->FreeList = Temp;
}

static int
Front(struct queue *Queue)
{
	assert(Queue != NULL);

	int Result = INT_MIN;
	if(Queue->Head)
	{
		Result = Queue->Head->Value;
	}

	return Result;
}

static void
PushBack(struct queue *Queue, int Value)
{
	assert(Queue != NULL);
	assert(Queue->FreeList != 0);

	struct queue_node *Node = Queue->FreeList;
	Queue->FreeList = Queue->FreeList->Next;

	Node->Next = 0;
	Node->Prev = 0;
	Node->Value = Value;

	if(Queue->Tail != 0)
	{
		Queue->Tail->Next = Node;
		Node->Prev = Queue->Tail;
	}
	Queue->Tail = Node;

	if(Queue->Head == 0)
	{
		Queue->Head = Node;
	}
}

static void
PopBack(struct queue *Queue)
{
	assert(Queue != NULL);

	struct queue_node *Temp = Queue->Tail;
	Queue->Tail = Queue->Tail->Prev;

	if(Queue->Tail != 0)
	{
		Queue->Tail->Next = 0;
	}
	else
	{
		Queue->Head = 0;
	}

	Temp->Value = -1;
	Temp->Prev = 0;
	Temp->Next = Queue->FreeList;
	Queue->FreeList = Temp;
}

static int
Back(struct queue *Queue)
{
	assert(Queue != NULL);

	int Result = INT_MIN;
	if(Queue->Tail != 0)
	{
		Result = Queue->Tail->Value;
	}

	return Result;
}

static int
Empty(struct queue *Queue)
{
	assert(Queue != NULL);

	int Result = (Queue->Head == 0 ? 1 : 0);
	return Result;
}

static void
FreeQueue(struct queue *Queue)
{
	assert(Queue != NULL);

	while(!Empty(Queue))
	{
		PopFront(Queue);
	}

	struct queue_node *Node = Queue->FreeList;
	while(Node != 0)
	{
		struct queue_node *Temp = Node;
		Node = Node->Next;
		free(Temp);
	}
}

static void
PrintQueue(struct queue *Queue)
{
	assert(Queue);

	for(struct queue_node *Node = Queue->Head; Node != 0; Node = Node->Next)
	{
		printf("%d ", Node->Value);
	}
	printf("\n");
}

static void
FindMaxValuesInArrayRanges(int Range, int *Array, int Length)
{
	struct queue Queue = NewQueue(Range+1);
	int Index = 0;
	for(; Index < Range; ++Index)
	{
		while((!Empty(&Queue)) &&
			  (Array[Index] >= Array[Back(&Queue)]))
		{
			PopBack(&Queue);
		}

		PushBack(&Queue, Index);
	}

	for(; Index < Length; ++Index)
	{
		printf("%d ", Array[Front(&Queue)]);

		while((!Empty(&Queue)) && Front(&Queue) <= Index - Range)
		{
			PopFront(&Queue);
		}

		while((!Empty(&Queue)) &&
			  (Array[Index] >= Array[Back(&Queue)]))
		{
			PopBack(&Queue);
		}

		PushBack(&Queue, Index);
	}

	printf("%d\n", Array[Front(&Queue)]);
	FreeQueue(&Queue);
}

int
main(void)
{
	int Array[] = {10, 5, 2, 7, 8, 7};
	int Range = 3;
	FindMaxValuesInArrayRanges(Range, Array, ArrayCount(Array));

	return 0;
}
