/**
 * This problem was asked by Microsoft.
 * 
 * Given a dictionary of words and a string made up of those words (no spaces),
 * return the original sentence in a list. If there is more than one possible
 * reconstruction, return any of them. If there is no possible reconstruction,
 * then return null.
 * 
 * For example, given the set of words 'quick', 'brown', 'the', 'fox', and the
 * string "thequickbrownfox", you should return ['the', 'quick', 'brown',
 * 'fox'].
 * 
 * Given the set of words 'bed', 'bath', 'bedbath', 'and', 'beyond', and the
 * string "bedbathandbeyond", return either ['bed', 'bath', 'and', 'beyond] or
 * ['bedbath', 'and', 'beyond'].
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static inline void
SwapCharPointers(char **A, char **B)
{
	char *Temp = *B;
	*B = *A;
	*A = Temp;
}

static void
QuickSort(char **Array, int Lower, int Upper)
{
	if(Lower >= Upper)
	{
		return;
	}

	char *PartitionValue = Array[Lower];
	int Partition = Lower;
	for(int Index = Lower + 1; Index < Upper; Index++)
	{
		if(strcmp(Array[Index], PartitionValue) < 0)
		{
			SwapCharPointers(&Array[Index], &Array[++Partition]);
		}
	}
	SwapCharPointers(&Array[Lower], &Array[Partition]);

	QuickSort(Array, Lower, Partition);
	QuickSort(Array, Partition + 1, Upper);
}

static int
CustomStrcmp(char *Match, char *Find)
{
	for(; *Match == *Find; ++Match, ++Find)
	{
		if(*Match == 0)
		{
			break;
		}
	}

	if(*Match == 0)
	{
		return 0;
	}

	return (*Find - *Match);
}

static char *
BinarySearch(char *String, char **WordList, int ListLength)
{
	char *Result = 0;
	int Low = 0;
	int High = ListLength;

	while(Low <= High)
	{
		int Mid = Low + (High - Low) / 2;
		int CmpResult = CustomStrcmp(WordList[Mid], String);
		if(CmpResult == 0)
		{
			Result = WordList[Mid];
			break;
		}
		
		if(CmpResult < 0)
		{
			High = Mid - 1;
		}
		else
		{
			Low = Mid + 1;
		}
	}

	return Result;
}

static char **
GetWordList(char *String, char **WordList, int ListLength)
{
	int ResultListMaxLength = ListLength;
	int ResultListLength = 0;
	char **ResultList = (char **)malloc(sizeof(*ResultList) * ResultListMaxLength);

	while(*String != 0)
	{
		char *Match = BinarySearch(String, WordList, ListLength - 1);
		if(Match != 0)
		{
			if(ResultListLength + 1 > ResultListMaxLength)
			{
				int NewMaxSize = ResultListMaxLength * 2;
				char **NewList = (char **)realloc(ResultList, sizeof(*NewList) * NewMaxSize);
				assert(NewList != NULL);  // just die

				ResultList = NewList;
				ResultListMaxLength = NewMaxSize;
			}

			ResultList[ResultListLength++] = Match;
			String += strlen(Match);
		}
		else
		{
			++String;
		}
	}
	ResultList[ResultListLength] = 0;

	return ResultList;
}

int
main(void)
{
#if 0
	char *WordList[] = {"quick", "brown", "the", "fox"};
	char *String = "thequickbrownfox";
#else
	char *WordList[] = {"bed", "bath", "bedbath", "and", "beyond"};
	char *String = "bedbathandbeyond";
#endif

	QuickSort(WordList, 0, ArrayCount(WordList));
	char **List = GetWordList(String, WordList, ArrayCount(WordList));
	for(char **Word = List; *Word != 0; ++Word)
	{
		printf("%s\n", *Word);
	}

	return 0;
}
