/**
 * This problem was asked by Twitter.
 * Implement an autocomplete system. That is, given a query string s and a set of all possible query strings, return all strings in the set that have s as a prefix.
 * For example, given the query string de and the set of strings [dog, deer, deal], return [deer, deal].
 * Hint: Try preprocessing the dictionary into a more efficient data structure to speed up queries.
 *
 * First I did this by taking the array and sorting it using QuickSort. Then I
 * just walk the array until I find strings that have the prefix string and
 * print those. Once I get to the end of matching strings I stop iterating
 * through the array and return the results.
 *
 * Next I implemented this dictionary using a Trie data structure. I insert
 * words into the trie and at each location where a word appears I drop a
 * pointer to the complete string (yes this doesn't work for strings that end on
 * the same location but that's fine for this). I then use the prefix to walk
 * the Trie and then iterate through all sub-tries grabbing each word.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static void
SwapStringPointers(char **A, char **B)
{
	char *Temp = *B;
	*B = *A;
	*A = Temp;
}

static void
QuickSortStringArray(char **Array, int Lower, int Upper)
{
	if(Lower >= Upper)
	{
		return;
	}

	char *PartitionValue = Array[Lower];
	int Partition = Lower;
	for(int At = Partition + 1; At < Upper; ++At)
	{
		if(strcmp(Array[At], PartitionValue) < 0)
		{
			SwapStringPointers(&Array[At], &Array[++Partition]);
		}
	}
	SwapStringPointers(&Array[Lower], &Array[Partition]);

	QuickSortStringArray(Array, Lower, Partition);
	QuickSortStringArray(Array, Partition + 1, Upper);
}

static char **
GetSuggestionsFromArray(char *Prefix, char **Array, int Length)
{
	assert(Array != 0);
	assert(*Array != 0);
	assert(Prefix != 0);

	int ResultMaxLength = 1;
	int ResultLength = 0;
	char **Result = (char **)malloc(sizeof(*Result) * ResultMaxLength);
	if(Result != 0)
	{
		int PrefixLength = strlen(Prefix);

		for(int Index = 0; Index < Length; ++Index)
		{
			int Comparison = strncmp(Array[Index], Prefix, PrefixLength);
			if(Comparison == 0)
			{
				if(ResultLength + 1 >= ResultMaxLength)
				{
					int NewMaxLength = ResultMaxLength * 2;
					char **Temp = (char **)realloc(Result, sizeof(*Temp) * NewMaxLength);
					if(Temp)
					{
						ResultMaxLength = NewMaxLength;
						Result = Temp;
					}
				}

				Result[ResultLength++] = Array[Index];
			}
			else if(Comparison < 0)
			{
				continue;
			}
			else
			{
				break;
			}
		}

		Result[ResultLength] = 0;
	}

	return Result;
}

// And now using a Trie - Probably the way they want me to answer this
struct trie
{
	char *Word;
	struct trie *Letters[26];
};

static struct trie *
BuildTrieFromArray(char **Array, int Length)
{
	struct trie *Root = (struct trie *)calloc(1, sizeof(struct trie));
	if(Root != 0)
	{
		for(int WordIndex = 0; WordIndex < Length; ++WordIndex)
		{
			struct trie *TrieWalker = Root;
			char *Word = Array[WordIndex];
			for(char *Letter = Word; *Letter != 0; ++Letter)
			{
				int Index = *Letter - 'a';
				if(TrieWalker->Letters[Index] == 0)
				{
					TrieWalker->Letters[Index] = (struct trie *)calloc(1, sizeof(struct trie));
					if(TrieWalker->Letters[Index] == 0)
					{
						break;
					}
				}

				TrieWalker = TrieWalker->Letters[Index];
			}
			TrieWalker->Word = Word;
		}
	}

	return Root;
}

static void
CharPtrArrayPush(char *Str, char **Array, int *Count, int *Size)
{
	assert(Array);
	assert(*Array);
	assert(Count);
	assert(Size);

	while(*Count + 1 >= *Size)
	{
		int NewSize = *Size * 2;
		char **NewArray = (char **)realloc(Array, sizeof(*Array) * NewSize);
		if(NewArray != 0)
		{
			*Size = NewSize;
			Array = NewArray;
		}
		else
		{
			assert(!"Not enough memory");
		}
	}

	if(*Count + 1 < *Size)
	{
		Array[*Count] = Str;
		*Count += 1;
	}
}

static void
_GetWordsInTrie(struct trie *Trie, char **Results, int *Length, int *MaxLength)
{
	if(Trie == 0)
	{
		return;
	}

	if(Trie->Word != 0)
	{
		CharPtrArrayPush(Trie->Word, Results, Length, MaxLength);
	}
	for(int i = 0; i < ArrayCount(Trie->Letters); ++i)
	{
		_GetWordsInTrie(Trie->Letters[i], Results, Length, MaxLength);
	}
}

static char **
GetSuggestionsFromTrie(char *Prefix, struct trie *Trie)
{
	assert(Trie != 0);

	int ResultMaxLength = 1;
	int ResultLength = 0;
	char **Result = (char **)malloc(sizeof(*Result) * ResultMaxLength);
	if(Result != 0)
	{
		struct trie *PrefixRoot = Trie;
		for(char *Letter = Prefix; *Letter != 0; ++Letter)
		{
			int Index = *Letter - 'a';
			if(PrefixRoot->Letters[Index] == 0)
			{
				break;
			}
			else
			{
				PrefixRoot = PrefixRoot->Letters[Index];
			}
		}

		_GetWordsInTrie(PrefixRoot, Result, &ResultLength, &ResultMaxLength);

		Result[ResultLength] = 0;
	}

	return Result;
}

int
main(void)
{
	char *Dictionary[] = {"dog", "deer", "deal", "apple", "cat", "zebra", "rock", "roll", "car", "destiny", "deep", "deed"};
	QuickSortStringArray(Dictionary, 0, ArrayCount(Dictionary));
	char **SuggestionsFromArray = GetSuggestionsFromArray("de", Dictionary, ArrayCount(Dictionary));
	for(char **Word = SuggestionsFromArray; *Word != 0; ++Word)
	{
		printf("%s\n", *Word);
	}

	printf("\n----------\n\n");

	struct trie *TrieDictionary = BuildTrieFromArray(Dictionary, ArrayCount(Dictionary));
	char **SuggestionsFromTrie = GetSuggestionsFromTrie("de", TrieDictionary);
	for(char **Word = SuggestionsFromTrie; *Word != 0; ++Word)
	{
		printf("%s\n", *Word);
	}

	return 0;
}
