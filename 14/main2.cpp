/**
 * This problem was asked by Google.
 * 
 * The area of a circle is defined as pr^2. Estimate p to 3 decimal places using
 * a Monte Carlo method.
 * 
 * Hint: The basic equation of a circle is x2 + y2 = r2.
 **/
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

// Using Intel Intrinsics function _rdrand16_step;
#include <immintrin.h>

struct thread_data
{
	int Simulations;
	double PiResult;
};

static DWORD WINAPI
GetMonteCarloSimulatedPI(void *Data)
{
	struct thread_data *ThreadData = (struct thread_data *)Data;

	double Pi = 0.0f;
	float Radius = 1.0f;
	int WithinRadius = 0;
	int Iterations = ThreadData->Simulations;

	int Iteration = 0;
	for(Iteration = 0; Iteration < Iterations; Iteration++)
	{
		unsigned short int RandX = 0;
		unsigned short int RandY = 0;
		_rdrand16_step(&RandX);
		_rdrand16_step(&RandY);
		float X = (RandX % 1000) / 1000.0f;
		float Y = (RandY % 1000) / 1000.0f;
		float Point = (float)(X*X + Y*Y);
		if(Point <= Radius)
		{
			++WithinRadius;
		}
	}

	ThreadData->PiResult = (double)((float)WithinRadius / (float)Iteration) * 4.0f;
	return 0;
}


int
main(int ArgCount, char **Args)
{
	if(ArgCount != 3)
	{
		fprintf(stderr, "Usage: %s [threads] [simulations]\n", Args[0]);
		return 1;
	}

	int ThreadCount = atoi(Args[1]);
	int Simulations = atoi(Args[2]);


	struct thread_data *ThreadData = (struct thread_data *)malloc(sizeof(*ThreadData) * ThreadCount);
	HANDLE *Threads = (HANDLE *)malloc(sizeof(*Threads) * ThreadCount);
	for(int ThreadIndex = 0; ThreadIndex < ThreadCount; ++ThreadIndex)
	{
		struct thread_data *Data = ThreadData + ThreadIndex;
		Data->Simulations = Simulations;
		Data->PiResult = 0;
		DWORD ThreadID;
		HANDLE ThreadHandle = CreateThread(0, 0, GetMonteCarloSimulatedPI, Data, 0, &ThreadID);
		Threads[ThreadIndex] = ThreadHandle;
	}

	WaitForMultipleObjects(ThreadCount, Threads, TRUE, INFINITE);

	double ColatedPi = 0.0f;
	for(int ThreadIndex = 0; ThreadIndex < ThreadCount; ++ThreadIndex)
	{
		CloseHandle(Threads[ThreadIndex]);
		printf("Thread %d returned %f\n", ThreadIndex + 1, ThreadData[ThreadIndex].PiResult);
		ColatedPi += ThreadData[ThreadIndex].PiResult;
	}
	ColatedPi = ColatedPi / ThreadCount;
	printf("ColatedPi = %f\n", ColatedPi);

	free(ThreadData);
	free(Threads);

	return 0;
}
