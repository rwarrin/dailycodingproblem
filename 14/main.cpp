/**
 * This problem was asked by Google.
 * 
 * The area of a circle is defined as pr^2. Estimate p to 3 decimal places using
 * a Monte Carlo method.
 * 
 * Hint: The basic equation of a circle is x2 + y2 = r2.
 **/
#include <stdio.h>
#include <stdlib.h>

// Using Intel Intrinsics function _rdrand16_step;
#include <immintrin.h>

static double
GetMonteCarloSimulatedPI(int Iterations)
{
	double Pi = 0.0f;
	float Radius = 1.0f;
	int WithinRadius = 0;

	for(int Iteration = 0; Iteration < Iterations; Iteration++)
	{
		unsigned short int RandX = 0;
		unsigned short int RandY = 0;
		_rdrand16_step(&RandX);
		_rdrand16_step(&RandY);
		float X = (RandX % 1000) / 1000.0f;
		float Y = (RandY % 1000) / 1000.0f;
		float Point = (float)(X*X + Y*Y);
		if(Point <= Radius)
		{
			++WithinRadius;
		}

		if(Iteration == 0) continue;
		Pi = (double)((float)WithinRadius / (float)Iteration) * 4.0f;
	}
	
	return Pi;
}

int
main(void)
{
	double Test1 = GetMonteCarloSimulatedPI(1000);
	printf("GetMonteCarloSimulatedPI(%d) = %f\n", 1000, Test1);

	double Test2 = GetMonteCarloSimulatedPI(30000);
	printf("GetMonteCarloSimulatedPI(%d) = %f\n", 30000, Test2);

	double Pi = 0.0f;
	for(int i = 0; i < 10; i++)
	{
		double NewPi = GetMonteCarloSimulatedPI(10000);
		Pi = (Pi + NewPi);
	}
	Pi = Pi / 10.0f;
	printf("10 runs of 10000 tests = %f\n", Pi);

	return 0;
}
