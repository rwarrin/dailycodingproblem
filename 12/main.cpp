#include <stdio.h>

// Uses a memoized solution where it remembers previous calculations so it
// doesn't have to repeat the calculations. Just using a memoization array of
// 100 here because its a toy program.
// My variable naming here is pretty poopy :)

static int MemoizedSteps[100];

static int
GetStepsRequired(int Stairs, int *StepSizes)
{
	if(Stairs < 0)
	{
		return 0;
	}

	if(Stairs == 0)
	{
		return 1;
	}

	if(MemoizedSteps[Stairs] != 0)
	{
		return MemoizedSteps[Stairs];
	}

	int Steps = 0;
	for(int *StepSize = StepSizes; *StepSize != 0; ++StepSize)
	{
		Steps += GetStepsRequired(Stairs - *StepSize, StepSizes);
	}
	MemoizedSteps[Stairs] = Steps;
	return Steps;
}

int
main(void)
{
	int StepSizes[] = {2, 3, 5, 0};
	int Stairs = 12;
	int Steps = GetStepsRequired(Stairs, StepSizes);
	printf("There are %d ways to go up %d stairs\n", Steps, Stairs);

	return 0;
}
