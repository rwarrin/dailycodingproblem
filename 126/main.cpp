/**
 * This problem was asked by Facebook.
 * Write a function that rotates a list by k elements. For example, [1, 2, 3, 4,
 * 5, 6] rotated by two becomes [3, 4, 5, 6, 1, 2]. Try solving this without
 * creating a copy of the list. How many swap or move operations do you need?
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Arary) (sizeof((Array)) / sizeof((Array)[0]))

static void
PrintArray(int *Array, int Length)
{
	for(int Index = 0; Index < Length; ++Index)
	{
		printf("%d, ", Array[Index]);
	}
	printf("\n");
}

static void
_Rotate(int *Array, int Left, int Right)
{
	assert(Array != 0);
	assert(Left <= Right);

	for( ; Left < Right; ++Left, --Right)
	{
		int Temp = Array[Left];
		Array[Left] = Array[Right];
		Array[Right] = Temp;
	}
}

static void
RotateArray(int *Array, int Length, int RotateAmount)
{
	assert(Array != 0);

	_Rotate(Array, 0, RotateAmount - 1);
	_Rotate(Array, RotateAmount, Length - 1);
	_Rotate(Array, 0, Length - 1);
}

int
main(void)
{
	int Array[] = {1, 2, 3, 4, 5, 6};
	int Rotate = 2;

	PrintArray(Array, ArrayCount(Array));
	RotateArray(Array, ArrayCount(Array), Rotate);
	PrintArray(Array, ArrayCount(Array));

	return 0;
}
