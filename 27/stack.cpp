#ifndef STACK_H

typedef struct pushdown_stack* StackT;

struct pushdown_stack
{
	item Items;

	int Count;
	void Push(item Item);
	item Pop();
	int Empty();
};

inline void
pushdown_stack::Push(item Item)
{
	assert(Item != NULL);
	Item->Next = this->Items;
	this->Items = Item;
	++this->Count;
}

inline item
pushdown_stack::Pop()
{
	item Result = 0;
	if(this->Items != NULL)
	{
		Result = this->Items;
		this->Items = this->Items->Next;
		--this->Count;
	}

	return Result;
}

inline int
pushdown_stack::Empty()
{
	int Result = ( (this->Items == 0) && (this->Count == 0) );
	return Result;
}

struct pushdown_stack *
NewStack()
{
	struct pushdown_stack *Stack = (struct pushdown_stack *)malloc(sizeof(*Stack));
	Stack->Count = 0;
	Stack->Items = 0;
	return Stack;
}


#define STACK_H
#endif
