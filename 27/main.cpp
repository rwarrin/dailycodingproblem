/**
 * This problem was asked by Facebook.
 * 
 * Given a string of round, curly, and square open and closing brackets, return
 * whether the brackets are balanced (well-formed).
 * 
 * For example, given the string "([])[]({})", you should return true.
 * 
 * Given the string "([)]" or "((()", you should return false.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

enum punctuation_type
{
	PunctuationType_Unknown = 0,
	PunctuationType_OpenBrace = '}',
	PunctuationType_CloseBrace = '}',
	PunctuationType_OpenBracket = '[',
	PunctuationType_CloseBracket = ']',
	PunctuationType_OpenParen = '(',
	PunctuationType_CloseParen = ')',
};

typedef struct item_struct* item;
struct item_struct
{
	punctuation_type Value;
	struct item_struct *Next;
};

#include "stack.cpp"

static inline void
PushPunctuation(StackT Stack, punctuation_type Type)
{
	assert(Stack != NULL);

	item Item = (item)malloc(sizeof(*Item));
	Item->Value = Type;
	Stack->Push(Item);
}

static inline punctuation_type
PopPunctuation(StackT Stack)
{
	assert(Stack != NULL);

	punctuation_type Result = PunctuationType_Unknown;

	item Item = Stack->Pop();
	if(Item != NULL)
	{
		Result = Item->Value;
		free(Item);
	}

	return Result;
}

static inline int
ExpectPunctuationTypeFromStack(StackT Stack, punctuation_type ExpectedType)
{
	int Result = 0;
	punctuation_type Type = PopPunctuation(Stack);
	if(Type == ExpectedType)
	{
		Result = 1;
	}
	return Result;
}

static int
IsPunctuationBalanced(char *String)
{
	assert(String != 0);

	StackT Stack = NewStack();

	int Result = 0;
	while(*String != 0)
	{
		char Character = *String;
		switch(Character)
		{
			case '(': { PushPunctuation(Stack, PunctuationType_OpenParen); } break;
			case '{': { PushPunctuation(Stack, PunctuationType_OpenBrace); } break;
			case '[': { PushPunctuation(Stack, PunctuationType_OpenBracket); } break;
			case ')':
			{
				if(!ExpectPunctuationTypeFromStack(Stack, PunctuationType_OpenParen))
				{
					return 0;
				}
			} break;
			case '}':
			{
				if(!ExpectPunctuationTypeFromStack(Stack, PunctuationType_OpenBrace))
				{
					return 0;
				}
			} break;
			case ']':
			{
				if(!ExpectPunctuationTypeFromStack(Stack, PunctuationType_OpenBracket))
				{
					return 0;
				}
			} break;
		}

		++String;
	}

	if(Stack->Empty())
	{
		Result = 1;
	}

	return Result;
}

static inline void
Test(char *String)
{
	int Balanced = IsPunctuationBalanced(String);
	printf("%-5s : \"%s\"\n", (Balanced == 1 ? "TRUE" : "FALSE"), String);
}

int
main(void)
{
	Test("([])[]({})");  // true
	Test("([)]");  // false
	Test("((()");  // false

	return 0;
}
