#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

#if 1
// O(2n)
static int *
GetProductArray(int *Array, int Length)
{
	assert(Array);

	int *Result = (int *)malloc(sizeof(int) * Length);
	if(Result != NULL)
	{
		int ZerosFound = 0;
		int Product = 1;
		for(int i = 0; i < Length; ++i)
		{
			if(Array[i] == 0)
			{
				++ZerosFound;
			}
			else
			{
				Product *= Array[i];
			}
		}

		for(int i = 0; i < Length; ++i)
		{
			if(ZerosFound > 1)
			{
				Result[i] = 0;
			}
			else
			{
				if(Array[i] == 0)
				{
					Result[i] = Product;
				}
				else
				{
					if(ZerosFound == 1)
					{
						Result[i] = 0;
					}
					else
					{
						Result[i] = Product / Array[i];
					}
				}
			}
		}
	}

	return(Result);
}
#else
// Follow-up: What if you can't use division?
// O(n^2)
static int *
GetProductArray(int *Array, int Length)
{
	assert(Array);

	int *Result = (int *)malloc(sizeof(int) * Length);
	if(Result != NULL)
	{
		for(int i = 0; i < Length; ++i)
		{
			int Product = 1;
			for(int j = 0; j < Length; ++j)
			{
				if(i != j)
				{
					Product *= Array[j];
				}
			}

			Result[i] = Product;
		}
	}

	return(Result);
}
#endif

static void
RunTest(int *Array, int Length)
{
	int *Result = GetProductArray(Array, Length);
	for(int i = 0; i < Length; ++i)
	{
		printf("%d, ", Result[i]);
	}
	printf("\n");
}

int
main(void)
{
	int Array1[] = {1, 2, 3, 4, 5};
	RunTest(Array1, ArrayCount(Array1));

	int Array2[] = {3, 2, 1};
	RunTest(Array2, ArrayCount(Array2));

	int Array3[] = {1};
	RunTest(Array3, ArrayCount(Array3));

	int Array4[] = {1, 2, 0, 4, 5};
	RunTest(Array4, ArrayCount(Array4));

	return 0;
}
