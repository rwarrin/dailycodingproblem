#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

/*
The following test should pass:

node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'
*/

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

struct atom
{
	char *String;
	int Length;
	struct atom *Next;
};

static struct atom *AtomTable[31];

static char *
InternString(char *String, int Length)
{
	int Hash = 0;
	for(int i = 0; i < Length; ++i)
	{
		Hash = Hash * 31 + String[i];
	}
	int Index = Hash % ArrayCount(AtomTable);

	struct atom *AtomPtr = AtomTable[Index];
	for(; AtomPtr != 0; AtomPtr = AtomPtr->Next)
	{
		if(Length == AtomPtr->Length)
		{
			if(strcmp(String, AtomPtr->String) == 0)
			{
				return AtomPtr->String;
			}
		}
	}

	struct atom *NewAtom = (struct atom *)malloc(sizeof(*NewAtom) + Length + 1);
	assert(NewAtom != 0);

	NewAtom->String = (char *)(NewAtom + 1);
	memcpy(NewAtom->String, String, Length);
	NewAtom->String[Length] = 0;
	NewAtom->Length = Length;
	NewAtom->Next = AtomTable[Index];
	AtomTable[Index] = NewAtom;

	return NewAtom->String;
}

struct node
{
	char *Value;
	struct node *Left;
	struct node *Right;
};

static char *Root = InternString("root", strlen("root"));
static char *Left = InternString("left", strlen("left"));
static char *Right = InternString("right", strlen("right"));

enum directions {
	Direction_STOP = 0,
	Direction_LEFT,
	Direction_RIGHT
};

static int GetDirection(char **DirectionString)
{
	int Result = Direction_STOP;

	char *End = *DirectionString;
	while((*End != 0) && (*End != '.'))
	{
		++End;
	}

	int Length = End - *DirectionString;
	if(strncmp(*DirectionString, Left, Length - 1) == 0)
	{
		Result = Direction_LEFT;
	}
	else if(strncmp(*DirectionString, Right, Length - 1) == 0)
	{
		Result = Direction_RIGHT;
	}

	if(*End != '.')
	{
		*DirectionString = End;
	}
	else
	{
		*DirectionString = End + 1;
	}

	return Result;
};

struct queue_node
{
	void *Data;
	struct queue_node *Next;
};

struct queue
{
	struct queue_node *Head;
	struct queue_node *Tail;
};

static void
Enqueue(struct queue *Queue, void *Data)
{
	assert(Queue != 0);

	struct queue_node *QueueNode = (struct queue_node *)malloc(sizeof(*QueueNode));
	QueueNode->Data = Data;
	QueueNode->Next = 0;

	if(Queue->Head == 0)
	{
		Queue->Head = QueueNode;
		Queue->Tail = QueueNode;
	}
	else
	{
		Queue->Tail->Next = QueueNode;
		Queue->Tail = QueueNode;
	}
}

static void *
Dequeue(struct queue *Queue)
{
	assert(Queue);
	void *Data = 0;

	if(Queue->Head != 0)
	{
		Data = Queue->Head->Data;
		struct queue_node *Temp = Queue->Head;
		Queue->Head = Queue->Head->Next;
		free(Temp);
	}

	return Data;
}

static struct node *
NodeCreate(char *Value)
{
	int ValueLength = strlen(Value);
	struct node *NewNode = (struct node *)malloc(sizeof(*NewNode) + ValueLength + 1);
	NewNode->Value = (char *)(NewNode + 1);
	memcpy(NewNode->Value, Value, ValueLength);
	NewNode->Value[ValueLength] = 0;
	NewNode->Left = 0;
	NewNode->Right = 0;

	return NewNode;
}

static void
TreeAdd(struct node **Node, char *Value)
{
	if(strcmp(Value, "root") == 0)
	{
		assert(*Node == NULL);
		*Node = NodeCreate(Value);
	}
	else
	{
		assert(*Node != NULL);
		struct node *Walker = *Node;

		char *DirectionString = Value;
		int Direction = Direction_STOP;
		while((Direction = GetDirection(&DirectionString)) != Direction_STOP)
		{
			if(Direction == Direction_LEFT)
			{
				if(Walker->Left == NULL)
				{
					Walker->Left = NodeCreate(Value);
				}

				Walker = Walker->Left;
			}
			else if(Direction == Direction_RIGHT)
			{
				if(Walker->Right == NULL)
				{
					Walker->Right = NodeCreate(Value);
				}

				Walker = Walker->Right;
			}
		}
	}
}

static char *
SerializeTree(struct node *Tree)
{
	// TODO(rick): This got ugly but it's late
	int BufferSize = 2;
	int BytesWritten = 0;
	char *Buffer = (char *)malloc(sizeof(*Buffer) * BufferSize);
	char *WritePtr = Buffer;

	struct queue Queue = {0};
	Enqueue(&Queue, Tree);

	struct node *Node = 0;
	while((Node = (struct node *)Dequeue(&Queue)) != 0)
	{
		Enqueue(&Queue, Node->Left);
		Enqueue(&Queue, Node->Right);

		int Length = strlen(Node->Value);
		while(BytesWritten + Length >= BufferSize)
		{
			int NewBufferSize = BufferSize * 2;
			char *NewBuffer = (char *)realloc(Buffer, sizeof(*NewBuffer) * NewBufferSize);
			if(NewBuffer)
			{
				Buffer = NewBuffer;
				WritePtr = Buffer + BytesWritten;
				BufferSize = NewBufferSize;
			}
		}
		memcpy(WritePtr, Node->Value, Length);
		WritePtr += Length;
		*WritePtr = ':';
		++WritePtr;
		BytesWritten += (Length + 1);
	}

	return Buffer;
}

static struct node *
DeserializeTree(char *String)
{
	char *Value = String;
	char *ValueEnd = String;

	struct node *Tree = 0;
	bool AtEnd = false;
	while((*Value != 0) && (AtEnd == false))
	{
		while((*ValueEnd != 0) && (*ValueEnd != ':'))
		{
			++ValueEnd;
		}
		if(*ValueEnd == 0)
		{
			AtEnd = true;
		}

		*ValueEnd = 0;
		TreeAdd(&Tree, Value);

		if(!AtEnd)
		{
			ValueEnd = ValueEnd + 1;
			Value = ValueEnd;
		}
	}

	return Tree;
}

static void
PrintTree(struct node *Tree)
{
	if(Tree == 0)
	{
		return;
	}

	PrintTree(Tree->Left);
	printf("%s\n", Tree->Value);
	PrintTree(Tree->Right);
}

int
main(void)
{
	struct node *Root = 0;
	TreeAdd(&Root, "root");
	TreeAdd(&Root, "left");
	TreeAdd(&Root, "left.left");
	TreeAdd(&Root, "right");

	PrintTree(Root);

	char *Serialized = SerializeTree(Root);
	printf("Serialized: %s\n", Serialized);
	struct node *Tree = DeserializeTree(Serialized);
	printf("--\n");
	PrintTree(Tree);
	printf("--\n");

	char *Value = DeserializeTree(SerializeTree(Root))->Left->Left->Value;
	printf("Value: %s\n", Value);

	return 0;
}
