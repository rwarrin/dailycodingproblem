/**
 * This problem was asked by Google.
 * 
 * Given an array of strictly the characters 'R', 'G', and 'B', segregate the
 * values of the array so that all the Rs come first, the Gs come second, and
 * the Bs come last. You can only swap elements of the array.
 * 
 * Do this in linear time and in-place.
 * 
 * For example, given the array ['G', 'B', 'R', 'R', 'B', 'R', 'G'], it should
 * become ['R', 'R', 'R', 'G', 'G', 'B', 'B'].
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static void
SwapChars(char *A, char *B)
{
	char Temp = *B;
	*B = *A;
	*A = Temp;
}

static void
Order(char *Array, int Length)
{
	char *Left = Array;
	char *Mid = Array;
	char *Right = Array + Length-1;

	while(Left <= Right)
	{
		if(*Mid == 'R')
		{
			SwapChars(Mid, Left);
			++Left, ++Mid;
		}
		else if(*Mid == 'G')
		{
			++Mid;
		}
		else if(*Mid == 'B')
		{
			SwapChars(Mid, Right);
			--Right;
		}
	}
}

int
main(void)
{
	char Array[] = {'G', 'B', 'R', 'R', 'B', 'R', 'G'};
	printf("%s\n", Array);
	Order(Array, ArrayCount(Array));
	printf("%s\n", Array);

	return 0;
}
