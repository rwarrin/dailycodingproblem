/**
 * This one was a little bit confusing just because the example wasn't very
 * clear. It wook a while to figure out what exactly was being asked here.
 **/

#include <stdio.h>
#include <stdlib.h>

struct node
{
	struct node *Left;
	struct node *Right;
	char Value;
};

static struct node *
NewNodeHelper(char Value, struct node *Left, struct node *Right)
{
	struct node *Result = (struct node *)malloc(sizeof(*Result));
	Result->Value = Value;
	Result->Left = Left;
	Result->Right = Right;
	return Result;
}

static int
CountUniValSubtrees(struct node *Tree, int *IsUnival)
{
	int Count = 0;
	if(Tree == 0)
	{
		*IsUnival = 1;
		return 0;
	}

	int IsLeftUnival = 0;
	int IsRightUnival = 0;
	int LeftCount = CountUniValSubtrees(Tree->Left, &IsLeftUnival);
	int RightCount = CountUniValSubtrees(Tree->Right, &IsRightUnival);

	*IsUnival = 0;
	Count = LeftCount + RightCount;
	if((IsLeftUnival == 1) && (IsRightUnival == 1))
	{
		if((Tree->Left != 0) && (Tree->Left->Value != Tree->Value))
		{
			return Count;
		}
		if((Tree->Right != 0) && (Tree->Right->Value != Tree->Value))
		{
			return Count;
		}

		*IsUnival = 1;
		return Count + 1;
	}

	return Count;
}

int
main(void)
{
	int IsUnival = 0;
	struct node *Tree = NewNodeHelper('0', NewNodeHelper('1', 0, 0), NewNodeHelper('0', NewNodeHelper('1', NewNodeHelper('1', 0, 0), NewNodeHelper('1', 0, 0)), NewNodeHelper('0', 0, 0)));
	printf("%d\n", CountUniValSubtrees(Tree, &IsUnival));

	struct node *Tree2 = NewNodeHelper('a', NewNodeHelper('a', 0, 0), NewNodeHelper('a', NewNodeHelper('a', 0, 0), NewNodeHelper('a', 0, NewNodeHelper('A', 0, 0))));
	printf("%d\n", CountUniValSubtrees(Tree2, &IsUnival));

	struct node *Tree3 = NewNodeHelper('a', NewNodeHelper('c', 0, 0), NewNodeHelper('b', NewNodeHelper('b', 0, 0), NewNodeHelper('b', 0, NewNodeHelper('b', 0, 0))));
	printf("%d\n", CountUniValSubtrees(Tree3, &IsUnival));

	struct node *Tree4 = NewNodeHelper('0', NewNodeHelper('1', 0, 0), NewNodeHelper('0', NewNodeHelper('1', NewNodeHelper('1', NewNodeHelper('0', 0, 0), 0), NewNodeHelper('1', 0, 0)), NewNodeHelper('0', 0, 0)));
	printf("%d\n", CountUniValSubtrees(Tree4, &IsUnival));
	
	return 0;
}
