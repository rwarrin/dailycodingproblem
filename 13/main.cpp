/**
 * This problem was asked by Amazon.
 *
 * Given an integer k and a string s, find
 * the length of the longest substring that contains at most k distinct
 * characters.
 *
 * For example, given s = "abcba" and k = 2, the longest substring
 * with k distinct characters is "bcb".
 **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#define MAX(A, B) ((A) > (B) ? (A) : (B))

struct frequency_table
{
	int Count;
	int UniqueCount;
	int Table[26];
};

static int
FrequencyTableAdd(struct frequency_table *FrequencyTable, char Character)
{
	assert(FrequencyTable != NULL);
	assert( (Character >= 'a') && (Character <= 'z') );

	int Index = Character - 'a';
	int Result = ++FrequencyTable->Table[Index];
	++FrequencyTable->Count;
	if(Result == 1)
	{
		++FrequencyTable->UniqueCount;
	}

	return Result;
}

static int
FrequencyTableRemove(struct frequency_table *FrequencyTable, char Character)
{
	assert(FrequencyTable != NULL);
	assert( (Character >= 'a') && (Character <= 'z') );

	int Index = Character - 'a';
	int Result = --FrequencyTable->Table[Index];
	--FrequencyTable->Count;
	if(Result == 0)
	{
		--FrequencyTable->UniqueCount;
	}

	return Result;
}

static int
GetLongestSubstringLength(int Distinct, char *String)
{
	assert(Distinct >= 0);
	assert(String != NULL);

	int MaxLength = 0;
	struct frequency_table FrequencyTable = {0};

	char *Start = String;
	char *End = String;
	while(*End != 0)
	{
		FrequencyTableAdd(&FrequencyTable, *End);
		if(FrequencyTable.UniqueCount > Distinct)
		{
			FrequencyTableRemove(&FrequencyTable, *Start);
			++Start;
		}

		MaxLength = MAX(MaxLength, FrequencyTable.Count);
		++End;
	}

	return MaxLength;
}

static void
Test(int Distinct, char *String)
{
	int Length = GetLongestSubstringLength(Distinct, String);
	printf("GetLongestSubstringLength(%d, \"%s\") = %d\n", Distinct, String, Length);
}

int
main(void)
{
	Test(2, "abcba");
	Test(1, "abcba");
	Test(3, "abcba");
	Test(0, "abcba");
	Test(2, "aa");
	Test(2, "");

	return 0;
}
