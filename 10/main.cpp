/**
 * Job Scheduler based on a priority queue using a linked list.
 * It works but doesn't ask for high resolution time and high priority so it
 * doesn't actually work perfectly but it accomplishes it's task.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <sys/timeb.h>

typedef void (*Function)(void *);

struct node
{
	int DelayMS;
	Function Closure;
	void *Data;
	struct node *Next;
};

static void
QueueJob(struct node **Queue, int DelayMS, Function Apply, void *Data)
{
	struct node *NewJob = (struct node *)malloc(sizeof(*NewJob));
	if(NewJob != NULL)
	{
		NewJob->DelayMS = DelayMS;
		NewJob->Closure = Apply;
		NewJob->Data = Data;
		NewJob->Next = 0;

		if(*Queue == NULL)
		{
			*Queue = NewJob;
		}
		else
		{
			struct node *Node = *Queue;
			for(; Node->Next; Node = Node->Next)
			{
				if(Node->Next->DelayMS > DelayMS)
				{
					break;
				}
			}

			NewJob->Next = Node->Next;
			Node->Next = NewJob;
		}
	}
}

static void
ProcessJobQueue(struct node **Queue, int TimeElapsed)
{
	struct node **NodePtr = Queue;
	while(*NodePtr != NULL)
	{
		struct node *Node = *NodePtr;
		Node->DelayMS -= TimeElapsed;
		if(Node->DelayMS <= 0)
		{
			printf("Running Job... ");
			Node->Closure(Node->Data);

			NodePtr = &(*NodePtr)->Next;
			if(Node == *Queue)
			{
				*Queue = Node->Next;
			}
			free(Node);
		}
		else
		{
			NodePtr = &(*NodePtr)->Next;
		}
	}
}

void
JobFunction1(void *Data)
{
	printf("JobFunction1: \"%s\"\n", (char *)Data);
}

int
main(void)
{
	struct _timeb TimeStart = {0};
	struct _timeb TimeEnd = {0};

	struct node *Queue = 0;
	QueueJob(&Queue, 10, JobFunction1, "Job 0");
	QueueJob(&Queue, 60, JobFunction1, "Job 1");
	QueueJob(&Queue, 30, JobFunction1, "Job 2");
	QueueJob(&Queue, 120, JobFunction1, "Job 3");
	QueueJob(&Queue, 500, JobFunction1, "Job 4");

	unsigned int TimeElapsed = 0;
	while(1)
	{
		_ftime(&TimeStart);
		ProcessJobQueue(&Queue, TimeElapsed);
		_ftime(&TimeEnd);
		TimeElapsed = TimeEnd.millitm - TimeStart.millitm;
	}

	return 0;
}
