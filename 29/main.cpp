/**
 * This problem was asked by Amazon.
 * 
 * Run-length encoding is a fast and simple method of encoding strings. The
 * basic idea is to represent repeated successive characters as a single count
 * and character. For example, the string "AAAABBBCCDAA" would be encoded as
 * "4A3B2C1D2A".
 * 
 * Implement run-length encoding and decoding. You can assume the string to be
 * encoded have no digits and consists solely of alphabetic characters. You can
 * assume the string to be decoded is valid.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

static void
RunLengthEncode(char *String)
{
	char *At = String;
	char *Scanner = String;

	while(*At != 0)
	{
		int Count = 0;
		while((*Scanner != 0) &&
			  (*Scanner == *At))
		{
			++Count;
			++Scanner;
		}
		printf("%d%c", Count, *At);
		At = Scanner;
	}
	printf("\n");
}

static void
RunLengthDecode(char *String)
{
	char *At = String;
	int Count = 0;
	while(*At != 0)
	{
		if( (*At >= '0') && (*At <= '9') )
		{
			int Number = *At - '0';
			Count = (Count * 10) + Number;
		}
		else
		{
			for(int i = 0; i < Count; ++i)
			{
				printf("%c", *At);
			}
			Count = 0;
		}
		++At;
	}
	printf("\n");
}

int
main(int ArgCount, char **Args)
{
	if(ArgCount != 3)
	{
		printf("Usage: %s -[encode|decode] [string]\n", Args[0]);
		return 1;
	}

	if(strcmp(Args[1], "-encode") == 0)
	{
		RunLengthEncode(Args[2]);
	}
	else if(strcmp(Args[1], "-decode") == 0)
	{
		RunLengthDecode(Args[2]);
	}

	return 0;
}
