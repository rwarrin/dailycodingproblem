#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))
#define Max(A, B) (((A) > (B)) ? (A) : (B))

static int
GetSum(int *Array, int Length)
{
	int CurrentMax = 0;
	int MaxBackOne = 0;
	int MaxBackTwo = 0;
	for(int Index = 0; Index < Length; ++Index)
	{
		if(Index == 0)
		{
			CurrentMax = Array[0];
		}
		else if(Index == 1)
		{
			CurrentMax = Max(Array[0], Array[1]);
		}
		else
		{
			CurrentMax = Max(MaxBackOne, Array[Index] + MaxBackTwo);
		}

		MaxBackTwo = MaxBackOne;
		MaxBackOne = CurrentMax;
	}

	return CurrentMax;
}

static void
Test(int *Array, int Length)
{
	printf("[");
	for(int Index = 0; Index < Length; ++Index)
	{
		printf("%d, ", Array[Index]);
	}

	int Sum = GetSum(Array, Length);
	printf("] Max Sum = %d\n", Sum);
}

int
main(void)
{
	int A1[] = {2, 4, 6, 2, 5};
	Test(A1, ArrayCount(A1));

	int A2[] = {5, 1, 1, 5};
	Test(A2, ArrayCount(A2));

	int A3[] = {5, 1, 20, 15, 1};
	Test(A3, ArrayCount(A3));

	return 0;
}
