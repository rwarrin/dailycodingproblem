/**
 * This is totally cheating I think... Sure space is constant but it's not the
 * same size as the array and the problem says "you can modify the array in
 * place" suggesting that I cannot allocate additional space. This also doesn't
 * seem like linear time if anything it's O(2n) because I'm essentially looping
 * over the length of the array twice.
 *
 * Modifying the array in-place suggests sorting which is not a linear time
 * operation.
 *
 * Okay I got the official solution and it cannot work in C++. This solution is
 * the closest and I can get to the optimal suggested solution.
 *
 * The largest positive number is between 1 and len(array) + 1.
 *
 * If the numbers are 13, 14, 16, 17 the official solution indexes into
 * Array[13] which is outside the bounds of the Array. 
 **/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <limits.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

// 0 - 4billion (good enough :))
// 488kB vs 4billion 4byte ints
char BitArray[500000];

#define SetValue(BitArray, Value) (BitArray[Value >> 3] |= (1 << (Value & 0x07)))
#define IsValueSet(BitArray, Value) (BitArray[Value >> 3] & (1 << (Value & 0x07)))
#define UnsetValue(BitArray, Value) (BitArray[Value >> 3] &= ~(1 << (Value & 0x07)))

static int
GetMaxMissingValue(int *Array, int Length)
{
	int Result = 0;

	int Smallest = INT_MAX;
	memset(BitArray, 0x00, ArrayCount(BitArray));
	for(int Index = 0; Index < Length; ++Index)
	{
		int Value = Array[Index];
		if(Value >= 0)
		{
			SetValue(BitArray, Value);
			if(Value < Smallest)
			{
				Smallest = Value;
			}
		}
	}

	int BitArrayMaxValue = ArrayCount(BitArray) * 8;
	for(int Index = Smallest; Index < BitArrayMaxValue; ++Index)
	{
		if(!IsValueSet(BitArray, Index))
		{
			Result = Index;
			break;
		}
	}

	return Result;
}

static inline void
TestFunction(int *Array, int Length)
{
	int Value = GetMaxMissingValue(Array, Length);
	printf("[");
	for(int Index = 0; Index < Length; ++Index)
	{
		if(Index == Length - 1)
		{
			printf("%d", Array[Index]);
		}
		else
		{
			printf("%d, ", Array[Index]);
		}
	}
	printf("] Missing: %d\n", Value);
}

int
main(void)
{
	int Array0[] = {3, 4, -1, 1};
	int Array1[] = {1, 2, 0};
	int Array2[] = {-1, -3, -5, -2};
	int Array3[] = {0};
	int Array4[] = {-1, -3, 0};
	int Array5[] = {13, 17, 14, 16};

	TestFunction(Array0, ArrayCount(Array0));
	TestFunction(Array1, ArrayCount(Array1));
	TestFunction(Array2, ArrayCount(Array2));
	TestFunction(Array3, ArrayCount(Array3));
	TestFunction(Array4, ArrayCount(Array4));
	TestFunction(Array5, ArrayCount(Array5));

	return 0;
}
