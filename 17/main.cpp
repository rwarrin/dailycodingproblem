/**
 * This problem was asked by Google.
 * 
 * Suppose we represent our file system by a string in the following manner:
 * 
 * The string "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext" represents:
 * 
 * dir
 *     subdir1
 *     subdir2
 *         file.ext
 * The directory dir contains an empty sub-directory subdir1 and a sub-directory
 * subdir2 containing a file file.ext.
 * 
 * The string "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext" represents:
 * 
 * dir
 *     subdir1
 *         file1.ext
 *         subsubdir1
 *     subdir2
 *         subsubdir2
 *             file2.ext
 *
 * The directory dir contains two sub-directories subdir1 and subdir2. subdir1
 * contains a file file1.ext and an empty second-level sub-directory subsubdir1.
 * subdir2 contains a second-level sub-directory subsubdir2 containing a file
 * file2.ext.
 * 
 * We are interested in finding the longest (number of characters) absolute path
 * to a file within our file system. For example, in the second example above,
 * the longest absolute path is "dir/subdir2/subsubdir2/file2.ext", and its
 * length is 32 (not including the double quotes).
 * 
 * Given a string representing the file system in the above format, return the
 * length of the longest absolute path to a file in the abstracted file system.
 * If there is no file in the system, return 0.
 * 
 * Note:
 * The name of a file contains at least a period and an extension.
 * The name of a directory or sub-directory will not contain a period.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

enum node_type
{
	NodeType_Unknown = 0,
	NodeType_RootParent,
	NodeType_Directory,
	NodeType_File,
};

struct node
{
	node_type NodeType;
	char Name[32];
	struct node *Next;
};

struct level
{
	struct level *Next;
	struct node *Children;
};

static struct node *
CreateNode(char *Name, int NameLength, node_type NodeType)
{
	struct node *NewNode = (struct node *)malloc(sizeof(*NewNode));
	if(NewNode != 0)
	{
		NewNode->NodeType = NodeType;
		NewNode->Children = 0;
		if(Name != 0)
		{
			strncpy(NewNode->Name, Name, NameLength);
			NewNode->Name[NameLength] = 0;
		}
	}
	return NewNode;
}

static struct node *
ParseDirectoryString(char *String)
{
	struct node *RootParent = 0;// CreateNode(0, 0, NodeType_RootParent);

	char *At = String;
	while(*At != 0)
	{
		if(*At == '\t')
		{
			while((*At != 0) &&
				  (*At == '\t'))
			{
				printf("Stepping directory down\n");
				++At;
			}
		}
		else
		{
			int IsFileName = 0;
			char *NameStart = At;
			while((*At != 0) &&
				  (*At != '\n'))
			{
				if(*At == '.')
				{
					IsFileName = 1;
				}
				++At;
			}
			printf("Inserting %s %.*s\n", (IsFileName ? "File" : "Directory"), (int)(At - NameStart), NameStart);
			++At;
		}
	}

	return RootParent;
}

int
main(void)
{
	char *TestStr = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext";
	//char *TestStr = "dir\n\tsubdir1\n\tsubdir2";
	printf("Input:\n%s\n---\n", TestStr);

	struct node *Tree = ParseDirectoryString(TestStr);

	return 0;
}
