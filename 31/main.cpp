/**
 * This problem was asked by Google.
 * 
 * The edit distance between two strings refers to the minimum number of
 * character insertions, deletions, and substitutions required to change one
 * string to the other. For example, the edit distance between �kitten� and
 * �sitting� is three: substitute the �k� for �s�, substitute the �e� for �i�,
 * and append a �g�.
 * 
 * Given two strings, compute the edit distance between them.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MIN2(A, B) (((A) < (B)) ? A : B)
#define MIN3(A, B, C) (MIN2(MIN2(A, B), C))

static int
GetMinimumEditDistance(char *String1, int String1Length, char *String2, int String2Length)
{
	int **Distances = (int **)malloc(sizeof(int *) * (String2Length + 1));
	for(int i = 0; i < String1Length + 1; i++)
	{
		Distances[i] = (int *)malloc(sizeof(int *) * (String1Length + 1));
	}

	for(int i = 0; i <= String1Length; i++)
	{
		for(int j = 0; j <= String2Length; j++)
		{
			if(i == 0)
			{
				Distances[i][j] = j;
			}
			else if(j == 0)
			{
				Distances[i][j] = i;
			}
			else if(String1[i - 1] == String2[j - 1])
			{
				Distances[i][j] = Distances[i - 1][j - 1];
			}
			else
			{
				Distances[i][j] = 1 + MIN3(Distances[i][j - 1],
										   Distances[i - 1][j],
										   Distances[i - 1][j - 1]);
			}
		}
	}

	return Distances[String1Length][String2Length];
}

int
main(void)
{
	char *String1 = "kitten";
	char *String2 = "sitting";
	int MinDistance = GetMinimumEditDistance(String1, strlen(String1), String2, strlen(String2));
	printf("%d\n", MinDistance);

	return 0;
}
