/**
 * This problem was asked by Facebook.
 * 
 * Implement regular expression matching with the following special characters:
 * 
 * . (period) which matches any single character
 * * (asterisk) which matches zero or more of the preceding element
 * That is, implement a function that takes in a string and a valid regular
 * expression and returns whether or not the string matches the regular
 * expression.
 * 
 * For example, given the regular expression "ra." and the string "ray", your
 * function should return true. The same regular expression on the string
 * "raymond" should return false.
 * 
 * Given the regular expression ".*at" and the string "chat", your function
 * should return true. The same regular expression on the string "chats" should
 * return false.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define true 1
#define false 0
typedef int bool32;

// Declare this function since Match and MatchMulti call each other
bool32 Match(char *Regex, char *String);

static bool32
MatchMulti(char Character, char *Regex, char *String)
{
	do
	{
		if(Match(Regex, String) == true)
		{
			return true;
		}
	} while((String != 0) &&
			((*String++ == Character) || (Character == '.')));

	return false;
}

static bool32
Match(char *Regex, char *String)
{
	if(*Regex == 0)
	{
		return true;
	}

	if(Regex[1] == '*')
	{
		return MatchMulti(Regex[0], Regex + 2, String);
	}

	if((String != 0) &&
	   ((*Regex == '.') || (*Regex == *String)))
	{
		return Match(Regex + 1, String + 1);
	}

	return false;
}

static bool32
RegexMatch(char *Regex, char *String)
{
	do
	{
		if(Match(Regex, String) == true)
		{
			return true;
		}
	} while(*String++ != '\0');

	return false;
}

static void
Test(char *Regex, char *String)
{
	printf(" Regex: \"%s\"\n", Regex);
	printf("String: \"%s\"\n", String);
	int Result = RegexMatch(Regex, String);
	printf(" Match: %s\n\n", (Result == true ? "TRUE" : "FALSE"));
}

int
main(void)
{
	Test("Hello", "Hello World");
	Test("Help", "Hello World");
	Test("ra.", "ray");
	Test("ra.s", "rats");
	Test("ra.s", "rays");
	Test("ra.s", "ray");
	Test("a*b", "ab");
	Test("a*b", "aaaab");
	Test("a*b", "bc");

	return 0;
}
