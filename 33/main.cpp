/**
 * This problem was asked by Microsoft.
 * 
 * Compute the running median of a sequence of numbers. That is, given a stream
 * of numbers, print out the median of the list so far on each new element.
 * 
 * Recall that the median of an even-numbered list is the average of the two
 * middle numbers.
 * 
 * For example, given the sequence [2, 1, 5, 7, 2, 0, 5], your algorithm should
 * print out:
 * 
 * 2
 * 1.5
 * 2
 * 3.5
 * 2
 * 2
 * 2
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

typedef struct ll_node* list;
struct ll_node
{
	int Value;
	struct ll_node *Next;
};

struct stream_list
{
	list List;
	int Length;
};

static void
ListInsert(int Value, list *List)
{
	list NewLink = (list)malloc(sizeof(*NewLink));
	NewLink->Value = Value;
	NewLink->Next = 0;

	if(*List == NULL)
	{
		*List = NewLink;
	}
	else if((*List)->Value > Value)
	{
		NewLink->Next = *List;
		*List = NewLink;
	}
	else
	{
		for(list Walker = *List; Walker != NULL; Walker = Walker->Next)
		{
			if(Walker->Next == NULL)
			{
				Walker->Next = NewLink;
				break;
			}
			else if(Walker->Next->Value > Value)
			{
				NewLink->Next = Walker->Next;
				Walker->Next = NewLink;
				break;
			}
		}
	}
}

static void
StreamAdd(int Value, struct stream_list *StreamList)
{
	assert(StreamList != NULL);
	ListInsert(Value, &StreamList->List);
	++StreamList->Length;
}

static float
GetStreamMedian(list List, int Length)
{
	assert(List != NULL);

	int Mid = Length / 2;
	list MidNode = List;
	for(int i = 0; i < Mid-1; ++i)
	{
		MidNode = MidNode->Next;
	}

	if(Length % 2 != 0)
	{
		return MidNode->Value;
	}
	else
	{
		return (MidNode->Value + MidNode->Next->Value) / 2.0f;
	}
}

static void
PrintList(list List)
{
	for(; List != 0; List = List->Next)
	{
		printf("%d ", List->Value);
	}
	printf("\n");
}

int
main(void)
{
	int Array[] = {2, 1, 5, 7, 2, 0, 5};

	struct stream_list StreamList = {0};
	for(int i = 0; i < ArrayCount(Array); ++i)
	{
		StreamAdd(Array[i], &StreamList);
		printf("%f\n", GetStreamMedian(StreamList.List, StreamList.Length));
	}

	return 0;
}
