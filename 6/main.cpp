#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>

struct node
{
	struct node *Link;
	int Value;
};

static inline struct node *
XORNodes(struct node *A, struct node *B)
{
	return (struct node *)(((uintptr_t)(A)) ^ ((uintptr_t)(B)));
}

static void
Add(struct node **List, int Value)
{
	struct node *NewNode = (struct node *)malloc(sizeof(*NewNode));
	if(NewNode != NULL)
	{
		NewNode->Value = Value;
		NewNode->Link = XORNodes(*List, 0);

		if(*List != 0)
		{
			struct node *Next = XORNodes((*List)->Link, 0);
			(*List)->Link = XORNodes(NewNode, Next);
		}

		*List = NewNode;
	}
}

static struct node *
Get(struct node *List, int Index)
{
	struct node *Node = List;
	struct node *Next = 0;
	struct node *Prev = 0;
	while((Node != 0) &&
		  (Index > 0))
	{
		Next = XORNodes(Prev, Node->Link);
		Prev = Node;
		Node = Next;
		--Index;
	}

	return Node;
}

enum
{
	Direction_Unknown = 0,
	Direction_Forward,
	Direction_Backward,
};

static void
PrintLinkedList(struct node *List, int Direction)
{
	// TODO(rick): Re-implement Direction

	assert(List != NULL);

	struct node *Node = List;
	struct node *Next = 0;
	struct node *Prev = 0;
	while(Node != 0)
	{
		printf("%d, ", Node->Value);
		Next = XORNodes(Prev, Node->Link);
		Prev = Node;
		Node = Next;
	}
	printf("\n");
}

int
main(void)
{

	struct node *List = 0;
	Add(&List, 0);
	Add(&List, 1);
	Add(&List, 2);
	Add(&List, 3);
	Add(&List, 4);
	Add(&List, 5);
	Add(&List, 6);
	Add(&List, 7);

	PrintLinkedList(List, Direction_Forward);

	printf("Get(List, 0) = %d\n", Get(List, 0)->Value);
	printf("Get(List, 1) = %d\n", Get(List, 1)->Value);
	printf("Get(List, 3) = %d\n", Get(List, 3)->Value);

	return 0;
}
