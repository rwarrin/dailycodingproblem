#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static int
PickRandomWithReservoirSampling(int *Array, int Length)
{
	int Result = 0;
	for(int i = 0; i < Length; i++)
	{
		int RandomInt = (rand() % (i + 1)) + 1;
		if(RandomInt == 1)
		{
			Result = Array[i];
		}
	}

	return Result;
}

int
main(void)
{
	srand(time(0));
	int BigStream[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
	printf("PickRandomWithReservoirSampling(Array, Length) = %d\n", PickRandomWithReservoirSampling(BigStream, ArrayCount(BigStream)));
	printf("PickRandomWithReservoirSampling(Array, Length) = %d\n", PickRandomWithReservoirSampling(BigStream, ArrayCount(BigStream)));
	printf("PickRandomWithReservoirSampling(Array, Length) = %d\n", PickRandomWithReservoirSampling(BigStream, ArrayCount(BigStream)));
	printf("PickRandomWithReservoirSampling(Array, Length) = %d\n", PickRandomWithReservoirSampling(BigStream, ArrayCount(BigStream)));
	printf("PickRandomWithReservoirSampling(Array, Length) = %d\n", PickRandomWithReservoirSampling(BigStream, ArrayCount(BigStream)));

	return 0;
}
