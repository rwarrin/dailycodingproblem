#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

// The O(n) running time and O(1) space solution. It works...
// But I'll do a solution using data structures.
static int
GetDecodeWayCount(char *Str)
{
	int Count = 0;

	while(*Str != 0)
	{
		if((*Str >= '1') && (*Str <= '9'))
		{
			++Count;
		}

		if((Str[1] != 0) &&
		   ((*Str == '1') || (*Str == '2')) &&
		   ((Str[1] >= '0') && (Str[1] <= '6')))
		{
			++Count;
			++Str;
		}

		++Str;
	}

	return Count;
}

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

struct hash_table
{
	int Count;
} HashTable[27];

static int
TableInsert(int Index)
{
	if(Index >= 1 && Index <= 26)
	{
		++HashTable[Index].Count;
		return 1;
	}

	return 0;
}

// O(n + m) running time and O(1) space.
// But at least it uses a hash table data structure?!?
static int
GetDecodeCount_HashTable(char *Str)
{
	memset(HashTable, 0x00, ArrayCount(HashTable) * sizeof(*HashTable));

	while(*Str != 0)
	{
		int Index = *Str - '0';
		TableInsert(Index);

		if(Str[1] != 0)
		{
			Index = (Index * 10) + (Str[1] - '0');
			if(TableInsert(Index) == 1)
			{
				++Str;
			}
		}

		++Str;
	}

	int Count = 0;
	for(int Index = 1; Index < ArrayCount(HashTable); ++Index)
	{
		Count += HashTable[Index].Count;
	}

	return Count;
}

int
main(void)
{
	char *Test1 = "111";
	printf("(GDWC)  %s can be decoded %d ways.\n", Test1, GetDecodeWayCount(Test1));
	printf("(GDCHT) %s can be decoded %d ways.\n", Test1, GetDecodeCount_HashTable(Test1));

	char *Test2 = "127";
	printf("(GDWC)  %s can be decoded %d ways.\n", Test2, GetDecodeWayCount(Test2));
	printf("(GDCHT) %s can be decoded %d ways.\n", Test2, GetDecodeCount_HashTable(Test2));

	return 0;
}
