/**
 * So this one seems really easy or I'm completely misunderstanding the problem.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct pair
{
	int X;
	int Y;
};

static inline pair
cons(int X, int Y)
{
	struct pair Result = {0};
	Result.X = X;
	Result.Y = Y;
	return Result;
}

static int
car(struct pair Pair)
{
	return Pair.X;
}

static int
cdr(struct pair Pair)
{
	return Pair.Y;
}

/**
 * Okay so I went to read what people were saying about this problem and it
 * sounds like what they're really expecting is a functional programming
 * solution so lets see what I can come up with.
 *
 * Honestly, what...? This seems completely pointless.
 *
 * This is the only thing I can think of what is actually being asked for.
 **/

static inline int
_cons(int X, int Y, int (*Func)(int, int))
{
	assert(Func != 0);

	int Result = Func(X, Y);
	return Result;
}

static int
_car(int X, int Y)
{
	return X;
}

static int
_cdr(int X, int Y)
{
	return Y;
}

/**
 * Can't wait to see what the actual solution is for this one. I'll revisit this
 * one if the official solution is any different than what I've done already.
 **/

int
main(void)
{
	printf("car(cons(3, 4)) = %d\n", car(cons(3, 4)));
	printf("cdr(cons(3, 4)) = %d\n", cdr(cons(3, 4)));

	printf("_cons(3, 4, _car) = %d\n", _cons(3, 4, _car));
	printf("_cons(3, 4, _cdr) = %d\n", _cons(3, 4, _cdr));

	return 0;
}
